#/bin/bash
echo '### Start building project ###';
./mvnw clean install
if [[ "$?" -ne 0 ]] ; then
  echo 'built project return error. Exit Now!!!'; exit $rc
fi
echo '### Start copying jar to instance ###';
scp -o ConnectTimeout=10 -C -i ~/.ssh/testinstance.pem target/*.jar ubuntu@ec2-34-229-145-234.compute-1.amazonaws.com:/home/ubuntu/application.jar
if [[ "$?" -ne 0 ]] ; then
  echo 'Copied jar return error. Exit Now!!!'; exit $rc
fi
echo '### Start reloading application ###';
ssh -i ~/.ssh/testinstance.pem ubuntu@ec2-34-229-145-234.compute-1.amazonaws.com docker-compose up -d --build
if [[ "$?" -ne 0 ]] ; then
  echo 'Reloaded application return error. Exit Now!!!'; exit $rc
fi