package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PubSubController {

    @GetMapping("/push-sub")
    @ResponseBody
    public ResponseEntity pushSub(String message) {
        System.out.println(message);
        return new ResponseEntity(HttpStatus.OK);
    }

}
