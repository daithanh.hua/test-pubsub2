package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestController {
    @GetMapping("/")
    public String home() {
        return "main.html";
    }

    @GetMapping(value = "/getScript")
    @CrossOrigin("*")
    public String getScript() throws InterruptedException {
        Thread.sleep(1000);
        return "scriptContainer.html";
    }
}
