fetch(window.location.href + "/getScript")
    .then(response => response.text())
    .then(structuredDataText => {
        const script = document.createElement('script');
        script.setAttribute('type', 'application/ld+json');
        script.textContent = structuredDataText;
        document.head.appendChild(script);
    });